package test;

import static org.junit.Assert.*;

import org.junit.Test;

import calculator.Calculator;

public class TestMul
{
	@Test
	public void testMul1() {
		Calculator c = new Calculator();
		assertEquals(c.mul(4,2), 8);
	}

	@Test
	public void testMul2() {
		Calculator c = new Calculator();
		assertEquals(c.mul(3,2), 6);
	}
}
