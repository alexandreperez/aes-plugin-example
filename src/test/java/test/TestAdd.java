package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import calculator.Calculator;

public class TestAdd
{
	@Test
	public void testAdd() {
		Calculator c = new Calculator();
		assertEquals(c.add(3,1), 4);
	}
}
