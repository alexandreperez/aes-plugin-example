# aes-maven-plugin example project

This is a sample project showing how the `aes-maven-plugin` should be declared in a project's `pom.xml`.

## Usage

After cloning this repository, run the following command:
```
mvn aes:test
```

It will download the maven plugin from an external repository, and execute it. After running the project's test cases, a report is created at `target/aes-report/`.